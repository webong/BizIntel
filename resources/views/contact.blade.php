@extends('layouts.master')

@section('content')
    <section id="">
            <div class="jumbotron jumbotron-fluid contact-hero">
                    <div class="container pt-5 mt-3">
                      <h1 class="display-4">Contact Us</h1>
                      <h5>You can find us at <i class="fa fa-map"></i></h5>
                      <p class=""> 
                         Suite 41, 24 Old Aba Road, near Artillery Bus Stop, <br>Rumuogba, Port Harcourt. Rivers State. Nigeria.</p>
                    </div>
                  </div>
    </section>

    <section id="">
        <div class="container pt-5 pb-5">
                <h3>Get in Touch</h3>
            <div class="row">
                <div class="col-sm-6 pt-3">
                    
                    <p>For feedback and inquiries , please fill out the form with your details and we would get back to you.</p>
                    <form action="{{ url('/contact') }}" method="POST">
                        @csrf
                           <div class="form-group pt-3">
                                <input type="text" name="name" class="form-control form-contact" placeholder="Name">
                              </div>
                              <div class="form-group pt-3">
                                    <input type="email" name="email" class="form-control form-contact" placeholder="Email">
                                  </div>
                                  <div class="form-group pt-3 pb-4">
                                        <textarea class="form-control form-contact" id="exampleFormControlTextarea1" name="msg" placeholder="Message" rows="3"></textarea>
                                      </div>
                                      <button type="submit" class="btn btn-lg" style="background-color: #239fbc; color: #fff;">SEND</button>
                          </form>

                </div>
                <div class="col-sm-6">
                     <div style="width: 100%">
                         <iframe width="100%" height="400" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3975.5787635312954!2d7.037905849099891!3d4.842153541748783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1069cd9a17332eaf%3A0x6aaa51e5f7278390!2s24+Old+Aba+Rd%2C+Obia%2C+Port+Harcourt!5e0!3m2!1sen!2sng!4v1503223913104" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Embed Google Map</a></iframe>   

                        {{--  https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3975.5787635312954!2d7.037905849099891!3d4.842153541748783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1069cd9a17332eaf%3A0x6aaa51e5f7278390!2s24+Old+Aba+Rd%2C+Obia%2C+Port+Harcourt!5e0!3m2!1sen!2sng!4v1503223913104 --}}
                     </div>
                     <div>
                            <p></p><span class="fa fa-map-marker pt-3" style="color:#33158C"></span> Suite 41, 24 Old Aba Road, near Artillery Bus Stop, Rumuogba, Port Harcourt. Rivers State. Nigeria.</p>

                     </div>
                </div>
            </div>
        </div>
    </section>

    <section id="subscribe">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10">
                            <h3>Subscribe Now for Loaded Business Building Tips
                                </h3>
                    </div>
                    <div class="col-sm-2">
                    <a href="#" button type="button" class="btn btn-subcribe d-sm-inline d-block ">Subscribe</a>
            </div>
                </div>
          </div>
        </section>
@endsection