

<!-- Modal -->
<div class="modal fade" id="consultancyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        {{-- <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5> --}}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4 class="mb-3 text-center">Need a Consultant send a request</h4>
        <form class="container px-3" method="POST" action="{{ url('/consultancy') }}">
          @csrf
          <div class="form-group">
            <label for="">Company Name</label>
            <input type="text" class="form-control" placeholder="" name="name">
           {{--  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
          </div>

          <div class="mb-3">
              <label for="email">Email </label>
              <input type="email" class="form-control" name="email" placeholder="you@example.com">
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
          </div>

          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="firstName">Company Url</label>
              <input type="url" class="form-control" name="url" placeholder="" value="" required>
              <div class="invalid-feedback">
                Valid Entry is required.
              </div>
            </div>
            <div class="col-md-6 mb-3">
              <label for="lastName">Reason <span class="text-muted">(Subject)</span></label>
              <input type="text" class="form-control" name="subject" placeholder="" value="" required>
              <div class="invalid-feedback">
                Valid Entry is required.
              </div>
            </div>
          </div>

           <div class="form-group">
            <label for="">Message</label>
            {{-- <input type="text" class="form-control" placeholder=""> --}}
            <textarea class="form-control" rows="4" name="msg"></textarea>
           {{--  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>