<!-- Modal -->
<div class="modal fade" id="financeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        {{-- <h5 class="modal-title text-center" id="exampleModalLongTitle">Get Offering for your Start Up</h5> --}}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="container">
              <div class="col-md-12 mx-2" >
                  <h4 class="mb-3 text-center">Get Offering for your Start Up</h4>
                  <form class="needs-validation" novalidate>
                    <div class="row">
                      <div class="col-md-6 mb-3">
                        <label for="firstName">Entry</label>
                        <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                        <div class="invalid-feedback">
                          Valid Entry is required.
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="lastName">Amount Needed</label>
                        <input type="number" class="form-control" id="lastName" placeholder="" value="" required>
                        <div class="invalid-feedback">
                          Valid Entry is required.
                        </div>
                      </div>
                    </div>

                     <div class="row">
                      <div class="col-md-6 mb-3">
                        <label for="firstName">Previous Funding</label>
                        <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                        <div class="invalid-feedback">
    
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="lastName">Contact Name</label>
                        <input type="number" class="form-control" id="lastName" placeholder="" value="" required>
                        <div class="invalid-feedback">
                          Valid Entry is required.
                        </div>
                      </div>
                    </div>


                    <div class="mb-3">
                      <label for="address">What does your company really do?</label> 
                      <textarea class="form-control" id="address"></textarea>
                      <div class="invalid-feedback">
                        Please enter your decision.
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-md-6 mb-3">
                        <label for="firstName">If You Have Traction/Revenue/Partnerships, Please Explain:</label>
                        {{-- <input type="text" class="form-control" id="firstName" placeholder="" value="" required> --}}
                        <textarea class="form-control" id="firstName" placeholder="" value="" rows="4" required></textarea>
                        <div class="invalid-feedback">
    
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="lastName">Email Address</label>
                        <input type="number" class="form-control" id="lastName" placeholder="" value="" required>
                        <div class="invalid-feedback">
                          Valid Entry is required.
                        </div>
                        <!---- Second Thing ------->
                        <label for="firstName">Phone Number</label>
                        <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                        <div class="invalid-feedback">
    
                        </div>
                      </div>
                    </div>
                    
                    <br>
                    {{--  --}}
                      <div class="row">
                      <div class="col-md-6 mb-3">
                        <label for="firstName">Press Links</label>
                        {{-- <input type="text" class="form-control" id="firstName" placeholder="" value="" required> --}}
                        <textarea class="form-control" id="firstName" placeholder="" value="" rows="4" required></textarea>
                        <div class="invalid-feedback">
    
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="lastName">Stage of Development</label>
                        <input type="number" class="form-control" id="lastName" placeholder="" value="" required>
                        <div class="invalid-feedback">
                          Valid Entry is required.
                        </div>
                        <!---- Second Thing ------->
                        <label for="firstName">Company URL</label>
                        <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                        <div class="invalid-feedback">
    
                        </div>
                      </div>
                    </div>

                    {{--  --}}
                    <br>
                     <div class="row">
                      <div class="col-md-6 mb-3">
                        <label for="firstName">Terms of Financing Round</label>
                        {{-- <input type="text" class="form-control" id="firstName" placeholder="" value="" required> --}}
                        <textarea class="form-control" id="firstName" placeholder="" value="" rows="4" required></textarea>
                        <div class="invalid-feedback">
    
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="lastName">Market / Industry</label>
                        <input type="number" class="form-control" id="lastName" placeholder="" value="" required>
                        <div class="invalid-feedback">
                          Valid Entry is required.
                        </div>
                        <!---- Second Thing ------->
                        <label for="firstName">Founder(s)</label>
                        <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                        <div class="invalid-feedback">
    
                        </div>
                      </div>
                    </div>

                    {{--  --}}
                    <br>
                     <div class="row">
                      <div class="col-md-6 mb-3">
                        <label for="firstName">Is there anything else we should consider in our review?</label>
                        {{-- <input type="text" class="form-control" id="firstName" placeholder="" value="" required> --}}
                        <textarea class="form-control" id="firstName" placeholder="" value="" rows="4" required></textarea>
                        <div class="invalid-feedback">
    
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="lastName">Incubator</label>
                        <input type="number" class="form-control" id="lastName" placeholder="" value="" required>
                        <div class="invalid-feedback">
                          Valid Entry is required.
                        </div>
                        <!---- Second Thing ------->
                        <label for="firstName">Notable Investor(s)</label>
                        <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                        <div class="invalid-feedback">
    
                        </div>
                      </div>
                    </div>

                    {{--  --}}

                    <div class="row">
                      <div class="col-md-8 mb-3">
                        <label for="firstName">How did you hear about MicroVentures?</label>
                        <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
                        <div class="invalid-feedback">
                          Valid Entry is required.
                        </div>
                      </div>
                      <div class="col-md-4 mb-3">
                        <label for="lastName">Upload Your Pitch Deck</label>
                        <input type="file" class="form-control" id="lastName" placeholder="" value="" required>
                        <small id="emailHelp" class="form-text text-muted">PDF,DOCX</small>
                        <div class="invalid-feedback">
                          Valid Entry is required.
                        </div>
                      </div>
                    </div>
                    
                    <hr class="mb-4">
                    <button class="btn service-btn btn-lg text-center" type="submit">Submit Request</button>
                  </form>
              </div>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
      </div>
    </div>
  </div>
</div>


{{-- <div class="mb-3">
                      <label for="address">Address</label>
                      <input type="text" class="form-control" id="address" placeholder="1234 Main St" required>
                      <div class="invalid-feedback">
                        Please enter your shipping address.
                      </div>
                    </div>

                    <div class="mb-3">
                      <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
                      <input type="text" class="form-control" id="address2" placeholder="Apartment or suite">
                    </div>

                    <div class="row">
                      <div class="col-md-5 mb-3">
                        <label for="country">Country</label>
                        <select class="custom-select d-block w-100" id="country" required>
                          <option value="">Choose...</option>
                          <option>United States</option>
                        </select>
                        <div class="invalid-feedback">
                          Please select a valid country.
                        </div>
                      </div>
                      <div class="col-md-4 mb-3">
                        <label for="state">State</label>
                        <select class="custom-select d-block w-100" id="state" required>
                          <option value="">Choose...</option>
                          <option>California</option>
                        </select>
                        <div class="invalid-feedback">
                          Please provide a valid state.
                        </div>
                      </div>
                      <div class="col-md-3 mb-3">
                        <label for="zip">Zip</label>
                        <input type="text" class="form-control" id="zip" placeholder="" required>
                        <div class="invalid-feedback">
                          Zip code required.
                        </div>
                      </div>
                    </div>
                    <hr class="mb-4">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="same-address">
                      <label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="save-info">
                      <label class="custom-control-label" for="save-info">Save this information for next time</label>
                    </div>
                    <hr class="mb-4">

                    <h4 class="mb-3">Payment</h4>

                    <div class="d-block my-3">
                      <div class="custom-control custom-radio">
                        <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required>
                        <label class="custom-control-label" for="credit">Credit card</label>
                      </div>
                      <div class="custom-control custom-radio">
                        <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" required>
                        <label class="custom-control-label" for="debit">Debit card</label>
                      </div>
                      <div class="custom-control custom-radio">
                        <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required>
                        <label class="custom-control-label" for="paypal">PayPal</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 mb-3">
                        <label for="cc-name">Name on card</label>
                        <input type="text" class="form-control" id="cc-name" placeholder="" required>
                        <small class="text-muted">Full name as displayed on card</small>
                        <div class="invalid-feedback">
                          Name on card is required
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="cc-number">Credit card number</label>
                        <input type="text" class="form-control" id="cc-number" placeholder="" required>
                        <div class="invalid-feedback">
                          Credit card number is required
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3 mb-3">
                        <label for="cc-expiration">Expiration</label>
                        <input type="text" class="form-control" id="cc-expiration" placeholder="" required>
                        <div class="invalid-feedback">
                          Expiration date required
                        </div>
                      </div>
                      <div class="col-md-3 mb-3">
                        <label for="cc-cvv">CVV</label>
                        <input type="text" class="form-control" id="cc-cvv" placeholder="" required>
                        <div class="invalid-feedback">
                          Security code required
                        </div>
                      </div>
                    </div> --}}