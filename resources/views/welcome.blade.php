@extends('layouts.master')

@section('content')
<section id="">
        <div class="jumbotron jumbotron-fluid wrapper">
            <div class="container pt-5 text-white">
              <h1 class="display-4">Bring Your Business<br> Idea to Life</h1>
              <div class="d-fex row-flex pt-5">
                <a href="#" button type="button" class="btn  btn-lg  mr-3 d-sm-inline d-block btn-hire text-center">Our Service</a>
              <a href="#" button type="button" class="btn btn-lg d-sm-inline d-block btn-get ">Get Updates</a>
            </div>
              </div>
              
          </div>
    </section>

     <section id=" services" class="mb-5">
        <div class="container ">
            <!-- <style>
                .what{
                    position: relative;
                    width: 50%;
                }

                .image {
                  opacity: 1;
                  display: block;
                  width: 100%;
                  height: auto;
                  transition: .5s ease;
                  backface-visibility: hidden;
                }

                .middle {
                  transition: .5s ease;
                  opacity: 0;
                  position: absolute;
                  top: 50%;
                  left: 50%;
                  transform: translate(-50%, -50%);
                  -ms-transform: translate(-50%, -50%);
                  text-align: center;
                }
                
                .middle a {
                    text-decoration: none;
                }

                .what:hover{
                  /* opacity: 0.3; */
                  background-color:rgba(0,0,0,0.5);
                  /* position:relative; */
                  /* top:100%; */
                  width:100%;
                  height:100%;
                }

                .what:hover .middle {
                  opacity: 1;
                  cursor: pointer;
                }

                .text {
                  background-color: #239fbc;
                  color: white;
                  font-size: 16px;
                  padding: 16px 32px;
                }
          </style> -->
            <!-- <h3 class="text-center"> <a href="services.blade.php">What We Do</a></h3> -->
            <!-- <hr class="hr"> -->
            <div class="row pt-4">
                <div class="what col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 text-center pt-5 pb-3">
                    <div class="image">
                         <!-- <center><i class="fa fa-money" style="font-size:72px"></i><br> -->
                             <img class="img-fluid " src="{{ asset('img/finance.svg') }} "></center>
                        <h6 class="pt-5 pb-2">Financial Solution For SMEs</h6>
                        <p class="">We intend to tackle this challenge head-on by providing needed finance for small businesses in a way that enable them to scale their business </p>
                        <a class="readdd pull-right" href="{{ url('/services#finance') }}">Learn More</a>
                    </div>
                    
                </div>
                <div class="what col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 text-center pt-5 pb-3">
                    <div class="image">
                        <!-- <center><i class="fas fa-theater-masks" style="font-size:72px"></i><br> -->
                            <img class="img-fluid" src="{{ asset('img/advice.svg') }}"></center> 
                        <h6 class="pt-5 pb-2">Advisory Services For SMEs</h6>
                        <p class=""> We provide general and bespoke consulting services to businesses, particularly those in the early phase</p>
                    
                    </div>
                    <a class="readdd pull-right" href="{{ url('/services#advisory') }}">Learn More</a>

                  
                </div>
                <div class="what col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 text-center pt-5 pb-3">
                    
                        <div class="image">
                            <center>
                        <img class="img-fluid" src=" {{ asset('img/consultant.svg') }}"></center> 
                            <h6 class="pt-5 pb-2">CSR Consultancy</h6>
                            <p class="">We have cognate experience in supporting businesses to plan and deliver a fit for purpose  corporate social responsibility project</p>
                    <a class="readdd pull-right" href="{{ url('/services#consultancy') }}">Learn More</a>
                        
                        </div>
                      
                    </div>
                
            </div>
        </div>
    </section>


    <section id="about" style="background: #fff;">
        <div class="container pt-5 pb-5 ">
            <h3 class="text-center ">About BizIntel</h3>
            <hr class="hr">
            <div class="row pt-4">
                <div class="col-sm-5">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"></iframe>
                      </div>
                </div>
                <div class="col-sm-7">
                    <!-- <h5>Team of brilliant individuals with the aim of making your business better.</h6> -->
                    <p class="text-justify">Bizintelng is your indispensable source for Business and investment consulting.We provide you with the latest breaking news and videos straight from business in perspectve coming from different sectors.</p>
                    <p class="text-justify">Business Intelligence Nigeria provides you with essential business information and business opportunities in Nigeria that you need to succeed in your business.
We promise to track and research every source to bring you those local economic  business  news and insights that really matter.
We also provide periodic analysis of the Nigeria business environment so you can better appreciate and take advantage of the information to advance your business.

The website is borne out of our frustration in searching for information that we needed to start our businesses.</p>
                   
                    <!-- <a href="{{url('/about')}}" type="button" class=" btn d-sm-inline d-block btn-view">Learn More</a> -->
                    <a href="{{url('/about')}}" button type="button" class="btn btn-vieww pull-right">More About BizIntel</a>

                </div>
            </div>
           
        </div>
    </section>

 <section id="subscribe">
            <div class="container p-5">
                <div class="m-3 text-center">
                    <!-- <div class="col-sm-10"> -->
                            <h3>We offer financial solutions to SMEs
                                </h3>
                <div class="d-block pt-3">
                                <a href="#" button type="button" class="btn btn-subcribe d-sm-inline d-block ">Learn More</a>
                </div>
                            </div>
                    <!-- <div class="col-sm-2"> -->
            </div>
            
          
        </section>
   
    <section id="">
        <div class="container pt-5">
        
            <h3 class="text-center"> Investment Portfolio </h4>
                <hr class="hr">
                <br>
                <style type="text/css">
                    .invest:hover {
                        cursor: pointer;
                        /*background-color: rgba(0, 0, 0, 0.16);*/
                      /* box-shadow:0px 3px 3px 3px rgb(200, 200, 200); */
                    }
                </style>
                @foreach($investments->chunk(2) as $investment)
                  <div class="row mt-5 my-5">
                      @foreach($investment as $p)  
                        <div class="col-md-6">
                            <div class="content">
                              <img class="img-fluid image w-100" src="{{$p->featured_image or 'img/investment-1.png'}}" style="height: 350px;">
                                <div class="overlay">
                                    <div class="text">
                                       <h1 class="bud p-500">{!! $p->title !!}</h1>
                                         <br><br>   
                                         <a class="over font-weight-bold pt-5 " href="{{ url('investment/'.$p->slug) }}">Read More</a> 
                                     </div>
                                </div>
                            </div> 
                        </div>
                      @endforeach
                  </div>
                @endforeach
                {{-- <div class="row ">
                    <div class="col-md-6">
                        <div class="content">
                        <img class="img-fluid image w-100" src="uploads/question.jpg" style="height: 350px;">
                            <div class="overlay">
                            <div class="text">
                            <h1 class="bud p-500">The Right Question to Ask For Business Startups</h1>
                              <br><br>   <a class="over font-weight-bold pt-5 " href="#">Read More</a> 
                             </div>
                            </div>
                            </div> 
                            </div>
                        
               
                            <div class="col-md-6">
                        <div class="content">
                        <img class="img-fluid  image w-100 "   src="uploads/funding.jpg" style="height: 350px;">
                            <div class="overlay">
                            <div class="text">
                            <h1 class="bud p-500">Right funding A most for your Business</h1>
                               
                              <br><br>   <a class="over font-weight-bold pt-5" href="#">Read More</a> 
                             </div>
                            </div>
                            </div> 
                              </div>
                </div>
                <div class="row ">
                    <div class="col-md-6 mt-4">
                        <div class="content">
                          <img class="img-fluid  image w-100"   src="uploads/ideas.jpg" style="height: 350px;">
                              <div class="overlay">
                              <div class="text">
                              <h1 class="bud p-500">Bring Your Business ideas to Life</h1>
                                  
                                <br><br>   <a class="over font-weight-bold pt-5" href="#">Read More</a> 
                               </div>
                              </div>
                              </div> 
                        </div>
                          <div class="col-md-6 mt-4">
                        <div class="content">
                        <img class="img-fluid  image w-100 "   src="uploads/news.jpg" style="height: 350px;">
                            <div class="overlay">
                            <div class="text">
                            <h1 class="bud p-500">The Go To Place for Your Business News</h1>
                                
                              <br><br>   <a class="over font-weight-bold pt-5" href="#">Read More</a> 
                             </div>
                            </div>
                            </div> 
                              </div> --}}
            </div>
        </div>
    </section>

    <section id="subscribee" style="background-color: #F9F9F9;"">
            <div class="container p-5 mt-5">
                <div class="m-3 text-center">
                    <!-- <div class="col-sm-10"> -->
                            <h3>Subscribe Now for Loaded Business Building Tips
                                </h3>
                <div class="d-block pt-3">
                                <a href="#" button type="button" class="btn btn-subcribe d-sm-inline d-block">Subscribe</a>
                </div>
                            </div>
                    <!-- <div class="col-sm-2"> -->
            </div>
            
          
        </section>

    <section id="blog">
        <div class="container pt-3 pb-5">
            <h3 class="text-center pt-5 "> <a href="blog.blade.php "class="text-b">Our Blog</a></h3>
            <hr class="hr">
            <div class="card-deck pt-4">

                @foreach($posts as $post)
                  <div class="card">
                      <img class="card-img-top" src="{{$post->featured_image or 'img/investment-1.png'}}" alt="Card image cap" height="200">
                      <div class="card-body">
                        <h6 class="card-title">{!! $post->title !!}</h6>
                        <p class="card-text small" style="margin-top: -5px; overflow: hidden; text-overflow: clip;">{!! str_limit($post->excerpt ,140)!!}</p>
                        <br>
                        <a class="readd" href="{{ url('blog/'.$post->slug) }}">Learn More</a>             
                    </div>
                  </div>

                @endforeach
                
              </div>
              <div class="pt-5">
              <a href="{{ url('/blog') }}" button type="button" class="btn btn-view d-block pull-right">View More</a></center>
                </div>
        </div>
    </section>
   
@endsection
