@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

        <h4 class="secondary"><strong>{{$subject}}</strong></h4>

        <p>{{$msg}}</p>

    @include('beautymail::templates.widgets.articleEnd')


    @include('beautymail::templates.widgets.newfeatureStart')

        {{-- <h4 class="secondary"><strong>Hello World again</strong></h4> --}}
        <p>{{$name}}</p>
        <div>
            Email:<i>{{$email}}</i>
        </div>
        <div>
            Company Url: <small>{{$url}}</small>
        </div>
        
             
    @include('beautymail::templates.widgets.newfeatureEnd')

@stop