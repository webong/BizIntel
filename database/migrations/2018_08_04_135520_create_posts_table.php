<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('posts', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('wp_id');
        //     $table->integer('user_id')->nullable();
        //     $table->string('title');
        //     $table->string('slug');
        //     $table->string('featured_image')->nullable();
        //     $table->string('featured')->nullable();
        //     $table->text('excerpt');
        //     $table->longtext('content');
        //     $table->string('format');
        //     $table->string('status');
        //     $table->string('publishes_at');
        //     $table->integer('category_id');
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
