<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/about', function () {
    return view('about');
});

Route::get('/services', function () {
    return view('services');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/blog', 'BlogController@index');

Route::get('/blog/category/{cat}', 'BlogController@category');

Route::get('/blog/tag/{tag}', 'BlogController@tag');

Route::get('/blog/{slug}', 'BlogController@post');

Route::get('/investment/{slug}', 'BlogController@post');


// Auth::routes();


Route::post('contact', function(Request $request) {
    $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
           //
           $beautymail->send('mails.contact', ['msg' => request()->msg, 'name' => request()->name, 'email' => request()->email,], function($message) 
            {
                $mail = 'admin@bizintelng.com';
                $message
                    ->from(request()->email)
                    ->to($mail)
                    ->subject(request()->subject);
            });

           return back()->with('success','Request Sent');
});
Route::post('/finace', function() {
    //
});

Route::post('/consultancy', function(Request $request) {
           $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
           //
           $beautymail->send('mails.consultancy', ['msg' => request()->msg, 'name' => request()->name, 'email' => request()->email, 'url' => request()->url, 'subject' => request()->subject], function($message) 
            {
                $mail = 'admin@bizintelng.com';
                $message
                    ->from(request()->email)
                    ->to($mail)
                    ->subject(request()->subject);
            });

           return back()->with('success','Request Sent');
});

Route::post('/advise', function() {
    $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
           //
           $beautymail->send('mails.consultancy', ['msg' => request()->msg, 'name' => request()->name, 'email' => request()->email, 'url' => request()->url, 'subject' => request()->subject], function($message) 
            {
                $mail = 'admin@bizintelng.com';
                $message
                    ->from(request()->email)
                    ->to($mail)
                    ->subject(request()->subject);
            });

           return back()->with('success','Request Sent');
});