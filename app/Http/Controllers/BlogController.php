<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \LeeOvery\WordpressToLaravel\Post;
use \LeeOvery\WordpressToLaravel\Category;
use App\Tag;
class BlogController extends Controller
{
	public function index(){

       $category = Category::whereSlug('must-read')->firstOrFail();
       $featured = Post::whereCategory($category->slug)
		             ->orderBy('published_at', 'desc')
		             ->get()
		             ->random(2);
       $cats = Category::all()->random(10);
       
       $newest = Post::orderBy('published_at', 'desc')->get()->random(8);
       $oldest = Post::orderBy('published_at', 'asc')->get()->random(8);

      //return $oldest; 
      return view('blog',[
       'featured' => $featured,
       'cats' => $cats,
       'newest' => $newest,
       'oldest' => $oldest,
      ]);  
	}

    public function post($slug)
    {
    	$post = Post::slug($slug)->firstOrFail();

    	//return $post;
    	$categories = Category::all();

    	$random = Post::orderBy('published_at', 'desc')->get()->random(4);

    	return view('post',[
    		'random' => $random,
    		'post'  => $post,
    		'categories' => $categories,
    	]);
    }

    public function category($category)
    {
    	$category = Category::whereSlug($category)->firstOrFail();

    	//dd($category);
        $categories = Category::all()->except('EDITORS PICK');
        $cats = Category::all()->random(11);

        //return $categories;
    	// to fetch newest 5 posts (paginated) by category slug (from above)...
		$posts = Post::whereCategory($category->slug)
		             ->orderBy('published_at', 'desc')
		             ->paginate(12);

		$random = Post::orderBy('published_at', 'desc')->get()->random(4);

		//return $posts;     
		return view('category',[
            'posts' => $posts,
            'random' => $random,
            'categories' => $categories,
            'cats' => $cats,
		]);        
    }

    public function tag($tag)
    {
    	// to fetch tag by tag slug, or fail...
		$tag = Post::createTagsModel()->whereSlug($tag)->firstOrFail();
        
        $tags = Tag::all();
        //return $tags;    
		// to fetch newest 5 posts (paginated) by tag slug (from above)...
		$posts = Post::whereTag($tag->slug)
		             ->orderBy('published_at', 'desc')
		             ->paginate(12);

		$random = Post::orderBy('published_at', 'desc')->get()->random(4);

		return view('tag',[
            'posts' => $posts,
            'tags' => $tags,
            'random' => $random,
		]);                 
	 }


}
